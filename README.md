# README #

### What is this repository for? ###

* `interface` generates java classes from proto files and then deployed to private maven repository. Services which need these classes can then import this project and use the files as needed.
* current version `0.0.1-SNAPSHOT`

### How do I get set up? ###

* Install java 11, maven
* The interface dependency below is hosted on my private maven repository.
```xml
<dependency>
  <groupId>com.innovationlabs</groupId>
  <artifactId>interface</artifactId>
  <version>1.0-SNAPSHOT</version>
</dependency>
```
Included in this project is a template file maven settings with the needed entries. You will just need to update the {{MAVEN_REPO_PASSWORD}} fields with the proper credentials and then point
your maven settings to this file in order to run this project locally with maven.

## Packaging and deploying
* After changes have been made, run `mvn clean package` to package
* Run `mvn clean deploy` to upload this project to the maven repository.

### Who do I talk to? ###

* Repo owner or admin
    * Robinson Mgbah - mgbahacho@aol.com